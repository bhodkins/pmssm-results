# some useful functions for selecting and plotting models
from scipy.interpolate import interp1d
import numpy as np
import pandas as pd
import math
import matplotlib.pyplot as plt
import matplotlib.colors as mplc
import matplotlib.patheffects as PathEffects
from matplotlib.lines import Line2D
import matplotlib as mpl
from plot_labels import label_map
import mplhep as hep
plt.style.use(hep.style.ATLAS)

# custom colormap
N = plt.cm.plasma.N       
cmaplist_magma = [plt.cm.magma(i) for i in range(N)]
    
cmaplist = [plt.cm.plasma(i) for i in range(N)]
cmaplist[0] = mpl.colors.to_rgba('black')
cmaplist[-1] = cmaplist_magma[-1] 
    
mycmap = mpl.colors.LinearSegmentedColormap.from_list('Custom cmap', cmaplist, N)
mycmap.set_bad('gainsboro')

# External constraints
external_constraint_bounds = {
# flavour - 2 sigma interval
'BF_b_to_s_gamma' : [3.11e-4, 3.87e-4],
'BF_Bs_to_mu_mu'  : [1.87e-9, 4.31e-9],
'BF_Bu_to_tau_nu' : [6.1e-5, 1.57e-4],
# EW precision
'delta_rho':[-0.0004, 0.0018],
'Invisible_Z_width':[0,2],
# W mass -- 2 sigma PDG avg excl. CDF with additional 6 MeV to account for EW top mass u/c
'm_W': [80.347, 80.407],#
# CDF W mass
#'value_m_w_mssm': [80.4087, 80.4583],
'DM_relic_density':[0,0.12],
'SI_nucleon_xsec__SCALED':'DD_contours/LUX_SI_nucleon.txt',
'SD_proton_xsec__SCALED':'DD_contours/PICO60_SDp.txt',
}

def read_contour(cFile):
	''' Read contour points from txt file '''
	with open(cFile,'r') as openFile:
		if cFile[-4:]=='.txt':
			x = []
			y = []
			with open(cFile, 'r') as txtFile:
				N=0
				for line in txtFile:
					N+=1
					if N==1: continue
					x.append(float(line.split(' ')[0]))
					y.append(float(line.split(' ')[1]))
		else:
			raise ValueError(f'{cFile} cannot be read -- please modify constraints.read_contour() to read this file type')
	return x, y

def interpolate_contour(contourFile):
	''' Returns function to interpolate contour values. Inputs outside the data range are assigned np.inf '''
	x, y = read_contour(contourFile)
	fInterp = interp1d(x, y, bounds_error=False, fill_value = np.inf)
	return fInterp

def DD_constraint(df, contourFile, xsec_column):
	''' Apply direct detection constraint given a contour file and the column for the corresponding DD xsec '''
	fInterp = interpolate_contour(contourFile)
	df = df[ df[xsec_column] <= fInterp(df['m_chi_10'])]
	return df

def apply_external_constraint(df, var):
	''' Apply external constraint associated with column {var} '''
	if var not in external_constraint_bounds:
		raise ValueError(f'Constraint variable {var} not recognised')
	X = external_constraint_bounds[var]
	if type(X) is str:
		df = DD_constraint(df, X, var)
	else:
		lower = X[0]
		upper = X[1]
		df = df[ (df[var]>=lower) & (df[var]<=upper)]
	return df

def external_constraints(df, DM=False, flavour=False, EW=False):
	'''
	Apply external constraints to dataframe df. Assign DM/flavour/EW = True to apply dark matter / flavour / electroweak precision constraints, respectively '''
	N0 = len(df)
	constraint_vars = []
	if flavour==True:
		constraint_vars+=['BF_b_to_s_gamma','BF_Bs_to_mu_mu','BF_Bu_to_tau_nu']
	if EW==True:
		constraint_vars+=['Invisible_Z_width','delta_rho','m_W']
	if DM==True:
		constraint_vars+=['DM_relic_density','SI_nucleon_xsec__SCALED','SD_proton_xsec__SCALED']
	for _var in constraint_vars:
		df = apply_external_constraint(df,_var)
	print(len(df),'/',N0,'models survive the external constraints')
	return df

def ATLAS_exclude(df):
	''' Select models not excluded by ATLAS '''
	N0 = len(df)
	df = df[ df['Final__CLs']>0.05]
	print(len(df),'/',N0,'models survive ATLAS Run 2')
	return df

def get_reco_level_models(df):
	''' Select models for which a reco-level MC sample already exists '''
	return df[ df['Reco_MC']==True]

def write_out_info(df, outFile, column):
	''' Write out model column to a txt file '''
	if outFile[-4:]!='.txt':
		raise ValueError('Please specify a .txt file to write out to')
	N=0
	with open(outFile,'w') as txt:
		for m in df.index:
			if df.loc[m,column] is None:
				print(f'WARNING: No {column} exists for model {m} -- skipping')
				continue
			else:
				txt.write(str(df.loc[m,column])+'\n')
				N+=1
	print(f'{N} {column}s written to {outFile}.')

def write_out_DSIDs(df, outFile):
	write_out_info(df[['DSID']].astype(int),outFile,'DSID')

def write_out_AODs(df, outFile):
	write_out_info(df,outFile,'AOD')

def write_out_PRWs(df, outFile):
	write_out_info(df,outFile,'PRW')

def write_out_EVNTs(df, outFile):
	write_out_info(df,outFile,'EVNT')

def write_out_SLHA(df, outFile):
	write_out_info(df,outFile,'SLHA')

def write_out_truth_ntuples(df, outFile):
	write_out_info(df,outFile,'Truth_ntuple')

def write_out_truth_filter_ntuples(df, outFile):
	noFilter = df['Truth_ntuple__FILTER'].isna()
	if np.sum(noFilter)>0:
	       print('WARNING:',np.sum(noFilter),'models do not have a truth-level sample produced with an event filter -- these will be skipped')
	write_out_info(df[~noFilter],outFile,'Truth_ntuple')

def atlas1D(df, var, excl = 'Final__CLs', saveTo = None, xlim=(0,1), nbins=10, 
		ymin=None, ymax=None, logy=False, lgd_loc='upper right', lgd_bbox = None, figsize=(6.4,6.4)):
	''' 
	Make a 1D distribution of a variable, before and after ATLAS Run 2.

	df : pandas dataframe
	var : dataframe column to plot
	excl : column used to determine exclusion ("excluded" means {excl} < 0.05")
	saveTo : path to save plot to, if None then plot is not saved
	xlim : x-axis range
	nbins : number of bins
	ymin : y axis minimum (set to 0 or 1 if left as None)
	ymax : y axis maximum (sensible value is calculated if left as None)
	logy : Set to True to use a log-scaled axis
	lgd_loc : matplotlib legend location
	lgd_bbox : used to set bbox_to_anchor in ax.legend() 
	figsize : matplotlib figsize
	'''

	fig, (ax,ax_f) = plt.subplots(2,1,gridspec_kw={'height_ratios':[5,1]},sharex=True, figsize=figsize)
	fig.patch.set_facecolor('white')
	bins = np.linspace(xlim[0],xlim[1],nbins+1)

	# histogram of all models
	n, _, _ = ax.hist(df[var].values, bins=bins, color='lightgrey', 
		label = 'Before ATLAS Run 2', range=xlim,histtype='stepfilled',stacked=False)

	# histogram of models left after ATLAS run 2
	n_left, _, _ = ax.hist(df[ df[excl]>0.05 ][var].values, bins=bins, color='midnightblue', 
			label = 'After ATLAS Run 2', range=xlim, histtype='stepfilled', linewidth=2, stacked=False)

	# redo main histogram for black edge
	ax.hist( bins[:-1], bins=bins, weights=n, edgecolor='black',
		range=xlim, histtype='step',linewidth=2, stacked=False)

	# plot ratio
	n_exc = np.array(n) - n_left
	with np.errstate(divide='ignore',invalid='ignore'):
		xv = np.array(n_exc)/np.array(n)
	yv = bins[:-1]+0.5*(bins[1]-bins[0])
	ax_f.plot(yv, xv, color='midnightblue')

	# Set y axis range
	if ymax is None:
		guess_ymax = math.ceil(np.amax(n)*1.2/50)*50
		if logy==True:
			guess_ymax*=5
		try:
			ax.set_ylim(top=guess_ymax)
		except: pass
	else:
		ax.set_ylim(top=ymax)
	if ymin:
		ax.set_ylim(bottom=ymin)
	elif logy==True:
		ax.set_ylim(bottom=1)

	# sort out axes and legend
	set_axis_labels(ax, xvar=None, yvar='Number of models',fontsize=17)
	set_axis_labels(ax_f, xvar=var, yvar='Frac. excl.',fontsize=17, yloc='center')
	ax.tick_params(axis='y',which='major',labelsize=14)
	ax_f.tick_params(axis='both',which='major',labelsize=14)
	ax_f.tick_params(axis='both',which='major',pad=7)
	fig.subplots_adjust(hspace=0.1)

	if logy==True:
		ax.set_yscale('log')
	ax_f.set_ylim(0,1)
	ax_f.set_xlim(xlim[0],xlim[1])
	ax.set_xlim(xlim[0],xlim[1])

	ax.legend(loc=lgd_loc, fontsize=14, bbox_to_anchor = lgd_bbox)

	atlText = '$\\sqrt{s} = \\mathdefault{13}\ \\mathrm{TeV}, \\mathdefault{140}\ \\mathrm{fb}^{\\mathdefault{-1}}$'
	hep.label.exp_label(exp='ATLAS', label='Internal', data=True, ax=ax, rlabel=atlText, loc=4)

	fig.tight_layout()

	if saveTo:
		fig.savefig(saveTo)
	plt.show()
	plt.close()

def scatter2D(df, xvar, yvar, saveTo=None, xlim=(1e1,1e3), ylim=(1e-7,1e1), xlog=True,ylog=True, 
	colorBy='LSP_type', vmin=None, vmax=None, clognorm=False,cmap=None,
	lgd_loc='lower left', badge_loc=None, 
	point_size=3,figsize=[6.4,6.4], dpi=100, labSize=16, lgdLabSize=14): 
	'''
	Make 2D scatter plot of 2 model variables

	df: dataframe
	xvar, yvar: name of variables to plot
	saveTo: path to save plot to
	xlim, ylim: x,y axis ranges
	xlog, ylog: True/False for using log-scaled axes

	colorBy: variable to use to color each point 
		* colorBy = LSP_type colors points by their LSP type.
		* colorBy = {A}_frac, where A = LSP / chi_{2,3,4}0 / chi_{1,2}p colors the points with RGB value set to the wino/higgsino/bino fraction of the electroweakino specified by `A`.
		* colorBy can also be set to any other column name to color the points by that column's values.
	vmin,vmax: min and max values on the color bar
	clognorm: True/False for using log scale for colors
	cmap: Use to specify your own color map (otherwise matplotlib default is used)

	lgd_loc: where to place the legend
	badge_loc: Location of lower left corner of dummy axis for custom ATLAS badge placement
	
	point_size : size of scatter points
	labSize : size of plot labels
	lgdLabSize : size of legend labels
	'''
	fig, ax = plt.subplots(figsize=figsize)

	# separate df by LSP type
	if colorBy=='LSP_type':
		bino_df = df[df['LSP_type']==1]
		wino_df = df[df['LSP_type']==2]
		higgsino_df = df[df['LSP_type']==3]

		ax.scatter(higgsino_df[xvar].values, higgsino_df[yvar].values, label='$\\tilde{H}$-like LSP', s=point_size, color='green')
		ax.scatter(wino_df[xvar].values, wino_df[yvar].values, label='$\\tilde{W}$-like LSP', s=point_size, color='red')
		ax.scatter(bino_df[xvar].values, bino_df[yvar].values, label='$\\tilde{B}$-like LSP', s=point_size, color='blue')			
		lgd1 = ax.legend(loc=lgd_loc, markerscale=5.0,fontsize=lgdLabSize)
	elif colorBy[-5:]=='_frac' and colorBy[-8:-5]!='ino':
		sparticle = colorBy[:-5]
		legend_elements = [Line2D([0],[0], marker='o', color='w',markerfacecolor=(1,0,0), label = 'Wino-like', markersize=15),
					Line2D([0],[0], marker='o', color='w',markerfacecolor=(0,1,0), label = 'Higgsino-like', markersize=15)]
		if sparticle == 'chi_1p' or sparticle == 'chi_2p':
			df[f'{sparticle}_Bino_frac'] = 0
		else:
			legend_elements.append(Line2D([0],[0], marker='o', color='w',markerfacecolor=(0,0,1), label = 'Bino-like', markersize=15))
		colors = df[[f'{sparticle}_Wino_frac',f'{sparticle}_Higgsino_frac',f'{sparticle}_Bino_frac']].values
		ax.scatter(df[xvar].values, df[yvar].values, c = colors, s = point_size)
		lgd1 = ax.legend(handles=legend_elements, loc=lgd_loc,fontsize=lgdLabSize,
			title =f'{ label_map[sparticle]} type', title_fontsize=lgdLabSize)
	else:
		norm=None
		z_vmin,z_vmax=vmin, vmax
		if clognorm:
			norm=mplc.LogNorm(vmin=vmin,vmax=vmax)
			z_vmin,z_vmax=None,None
		z = ax.scatter(df[xvar].values, df[yvar].values, c = df[colorBy].values,
			cmap=cmap,
			s = point_size, norm=norm,vmin=z_vmin,vmax=z_vmax)
		if colorBy in label_map:
			colorlabel = label_map[colorBy]
		else:
			colorlabel=colorBy
		cbar = fig.colorbar(z)
		cbar.set_label(label=colorlabel, size=labSize)
		cbar.ax.tick_params(labelsize=labSize)

	if ylog==True:
		ax.set_yscale('log')
	if xlog==True:
		ax.set_xscale('log')

	plt.xticks(fontsize=labSize)
	plt.yticks(fontsize=labSize)

	ax.set_ylim(ylim)
	ax.set_xlim(xlim)

	set_axis_labels(ax, xvar=xvar, yvar=yvar,fontsize=labSize)
	cut_text = {True:'ON', False:'OFF'}
	atlText = '$\\sqrt{s} = \\mathdefault{13}\ \\mathrm{TeV}, \\mathdefault{140}\ \\mathrm{fb}^{\\mathdefault{-1}}$'
	if badge_loc is not None:
		# trick to put ATLAS label in bottom left - create invisible inset axes
		ax_dummy = ax.inset_axes([badge_loc[0],badge_loc[1],0.5,0.25])
		hep.label.exp_label(exp='ATLAS', label='Internal', data=True,ax=ax_dummy, rlabel=atlText,loc=4)
		ax_dummy.axis('off')
	else:
		hep.label.exp_label(exp='ATLAS', label='Internal', data=True,ax=ax,rlabel=atlText,loc=1)
	ax.tick_params(axis='both',which='major',pad=7)
	plt.tight_layout()
	if saveTo:
		fig.savefig(saveTo, dpi=dpi,bbox_inches = "tight")
	plt.show()
	plt.close()

def set_axis_labels(ax, xvar=None, yvar=None,fontsize=14, yloc='top', xloc='right', xlabelpad=None, ylabelpad=None):
	''' 
	Set appropriate axis (ax) labels for each x and y variable 

	ax: MatPlotLib axis object
	latex_dict: dictionary mapping from xvar and yvar to a latex'd label
	xvar, yvar: name of variables plotted on each axis
	fontsize: size of labels	
	'''
	if xvar:
		if xvar in label_map:
			ax.set_xlabel(label_map[xvar], fontsize=fontsize, loc=xloc, labelpad = xlabelpad)
		else:
			ax.set_xlabel(xvar,fontsize=fontsize, loc=xloc, labelpad = xlabelpad)
	if yvar:
		if yvar in label_map:
			ax.set_ylabel(label_map[yvar],fontsize=fontsize, loc=yloc, labelpad = ylabelpad)
		else:
			ax.set_ylabel(yvar,fontsize=fontsize, loc=yloc, labelpad = ylabelpad)

def binned2D(df, xvar, yvar, xlim=(1e1,1e3), ylim=(1e-7,1e1), nxbins=15,nybins=15, 
		xedges=None, yedges=None, logx=False, logy=False,nxticks=None, nyticks=None, 
		plotPath=None, figsize=(8,6),dpi=200,tickpad = None,
		labSize=20, lgdLabSize=14, lgdLoc='upper left',

		zFunc='exclusion', zLabel='Fraction of models excluded',cmap=None,
		annFunc='count', annLabel='Bins labelled with total number of models', noZeroAnn=True, annSize=10,

		excl='Final__CLs', 
		):
	'''
	Make binned 2D plot 

	df: pandas dataframe
	xvar,yvar: name of variables to bin
	xlim, ylim: x,y axis ranges
	nxbins,nybins: how many bins to use for each axis
	xedges, yedges : supply custom bin edges for non-uniform binning (use instead of xlim, ylim, nxbins and nybins)
	logx, logy : flags for log-scale axes
	n{x/y}ticks: optionally set number of x axis label ticks to show (useful when large numbers are overlapping)

	plotPath: path to save plot to (if specified)
	figsize, dpi: matplotlib subplot figsize and dpi
	tickpad: add padding between axes and labels
	labSize: size of matplotlib axis and tick labels
	lgdLabSize: size of legend labels
	lgdLoc: matplotlib keyword for legend location

	zFunc:
		- string to specify the function to apply to calculate bin color values
		- current values that work: 'exclusion', 'count' and '{max/mean/min}_{varName}'
	zLabel: label for colorbar
	annFunc:
		- string to specify the function to apply to calculate bin annotation
		- current values that work: None, 'exclusion', 'count' and '{max/mean/min}_{varName}'
	annLabel: label to add to ATLAS badge to explain bin annotation
	noZeroAnn: True/False to leave bins with label=0 blank
	annSize: size of bin labels
	'''

	fig, ax = plt.subplots(figsize=figsize)

	def _wrapped_funcs(_fString):

		def myFunction(x):
			if len(x)==0:
				return float('nan')
			if _fString=='exclusion':
				return len( x[x[excl]<0.05]) / len(x)
			elif 'mean_' in _fString:
				zvar = _fString.split('mean_')[1]
				return x[zvar].mean()
			elif 'max_' in _fString:
				zvar = _fString.split('max_')[1]
				return x[zvar].max()
			elif 'min_' in _fString:
				zvar = _fString.split('min_')[1]
				return x[zvar].min()
			elif _fString=='count':
				return len(x)
			elif _fString=='none' or _fString is None:
				return ''
			else:
				return 0
		return myFunction

	# bin dataframe and calculate value for each bin
	df = df.copy()
	if xedges is None:
		if logx:
			xedges = np.logspace(np.log10(xlim[0]),np.log10(xlim[1]),nxbins+1)
		else:
			xedges = np.linspace(xlim[0],xlim[1],nxbins+1)
	if yedges is None:
		if logy:
			yedges = np.logspace(np.log10(ylim[0]),np.log10(ylim[1]),nybins+1)
		else:
			yedges = np.linspace(ylim[0],ylim[1],nybins+1)
	xcut = pd.cut(df[xvar],xedges)
	ycut = pd.cut(df[yvar],yedges)
	df.loc[:,'xbins'] = pd.Categorical(xcut)
	df.loc[:,'ybins'] = pd.Categorical(ycut)
	df['xbins'] = pd.Categorical(df['xbins'])
	df['ybins'] = pd.Categorical(df['ybins'])

	lambdaFunc = _wrapped_funcs(zFunc)

	F = df.groupby(['xbins','ybins']).apply(lambda x: lambdaFunc(x))
	F = F.reindex(pd.MultiIndex.from_product([xcut.cat.categories, ycut.cat.categories])).unstack()
	xedgesFinal = [x.left for x in F.index]+[F.index[-1].right]
	yedgesFinal = [y.left for y in F.columns]+[F.columns[-1].right]

	# get values for annotation / circle sizing
	if annFunc is not None:
		annLambdaFunc = _wrapped_funcs(annFunc)
		annVals = df.groupby([xcut, ycut]).apply(lambda x: annLambdaFunc(x))
		annVals = annVals.reindex(pd.MultiIndex.from_product([xcut.cat.categories, ycut.cat.categories])).unstack()
	if cmap==None: 
		_cmap = mycmap
	else:
		_cmap = cmap
	im = ax.pcolormesh(xedgesFinal,yedgesFinal,F.transpose(),shading='auto',vmin=0,vmax=max(1, int(np.nanmax(F.max(skipna=True)))),cmap = _cmap )

	if logx: ax.set_xscale('log')
	if logy: ax.set_yscale('log')

	# annotate plot
	if annFunc is not None:
		for x in annVals.index:
			for y in annVals.columns:
				annNum = np.nan_to_num(annVals.loc[x,y])
				if annNum==0 and noZeroAnn==True: continue
				if annNum>=1:
					annNum=int(annNum)
					shift=4
				elif annNum>=0.1:
					annNum=round(annNum,2)
					shift=6
				elif annNum>=0.001:
					annNum=round(annNum,3)
					shift=10
				else:
					annNum= 0
					shift=4
				xLoc = x.left + (x.right-x.left)/shift
				yLoc = y.left + (y.right-y.left)/shift
				ann = ax.annotate(str(annNum), (xLoc, yLoc),  fontsize=annSize, color='white')
				ann.set_path_effects([PathEffects.withStroke(linewidth=2, foreground='k')])

	# other plotting things
	ax.set_xlim(left=xlim[0],right=xlim[1])
	if nxticks:
		ax.locator_params(axis='x',nbins=nxticks)
	if nyticks:
		ax.locator_params(axis='y',nbins=nyticks)

	cbar=fig.colorbar(im)
	cbar.set_label(label=zLabel,size=labSize)
	cbar.ax.tick_params(labelsize=labSize)
	atlStr = '$\\sqrt{s}=\\mathdefault{13}\\,\\mathrm{TeV}$, $\\mathdefault{140}\\,\\mathrm{fb}^{\\mathdefault{-1}}$'
	if len(annLabel)>0: atlStr+=', '+annLabel

	ax_dummy = ax.inset_axes([-0.03,1.,0.8,0.1])
	hep.label.exp_label(exp='ATLAS', label='Internal', data=True, ax=ax_dummy,rlabel=atlStr,loc=4)
	ax_dummy.axis('off')

	ax.set_ylim(bottom=ylim[0],top=ylim[1])
	plt.xticks(fontsize=labSize)
	plt.yticks(fontsize=labSize)
	set_axis_labels(ax, xvar=xvar, yvar=yvar,fontsize=labSize)

	if tickpad is not None:
		ax.tick_params(axis='both',which='major',pad=tickpad)

	plt.tight_layout()
	if plotPath:
		fig.savefig(plotPath, dpi=dpi)
	plt.show()
	plt.close()

