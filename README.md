# pMSSM scan results
This repo is intended to give the SUSY group access to the models and results of the EWK pMSSM scan ([SUSY-2020-15](https://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/PAPERS/SUSY-2020-15/)). 

Two scans were performed: 
* The EWKino scan is a random scan of the pMSSM parameters relevant to electroweak production with uniform priors. It includes models with bino-, wino- and higgsino-like neutralino LSPs.
* The Bino-DM scan is similar, but contains only models with a bino-like LSP that satisfy the requirement that the prediction dark matter relic density is less than or equal to the observed value 0.12.

The results are contained in two CSV files:
* `/eos/atlas/atlascerngroupdisk/phys-susy/pMSSM/Run2/EW_Run2_data/EWKino.csv` 
* `/eos/atlas/atlascerngroupdisk/phys-susy/pMSSM/Run2/EW_Run2_data/Bino-DM.csv`

These contain the results for each scan along with information about each model. This includes relevant electroweakino masses, branching ratios and mixing as well as DSIDs, paths to truth-level ntuples, SLHA files and DSIDs where available. These CSV files can be downloaded and read with your preferred software -- information on what each column contains is provided below. My preference is to read these files as a pandas dataframe, and the included example notebook provides an example of how to do this. Some helper functions for selecting and plotting sets of models are also provided in `utilities.py`.

All models included in the CONF-NOTE, prior to the application of external constraints, are included in `EWKino.csv` and `Bino-DM.csv`. The function `utilities.external_constraints()` can be used to select models that satsify dark matter and/or flavour and/or EW precision constraints:

`df_constrained = utilities.external_constraints(df, DM=True, flavour=True, EW=True)`

The idea is that you can use the information provided here to select slices of (unexcluded) models that are relevant to a particular phase space of interest and have direct access to the corresponding SLHA files, truth-level ntuples and AODs without necessarily having to go through the pMSSM team. Of course, I and the rest of the pMSSM team are also very happy to assist.

Please send comments, feedback and report issues to ben.hodkinson@cern.ch.

# Set-up notebook on SWAN
[![Open in SWAN][image]][hyperlink]

[image]: open_in_swan.svg

[hyperlink]: https://swan-k8s.cern.ch/user-redirect/download?projurl=https://gitlab.cern.ch/bhodkins/pmssm-results.git

Launch `example.ipynb`

The notebook can also be run locally via jupyter.

# Plotting functions

I have included three plotting functions that may be useful, though of course there are many more interesting plots that one could make. This is based on the code used to produce the paper, so you can produce similar plots. `plot_labels.py` contains a dictionary mapping from the column names to latex strings for nice axis labels.

`utilities.atlas1D(df, var, excl)` : Make a 1D plot of the variable in column `{var}` for all models and those satisfying `excl>0.05` (ie. that evade exclusion). By default, `excl=Final__CLs` such that the overall ATLAS Run 2 exclusion is plotted. This could be changed to eg. `2L0J__ObsCls` to just plot the 2L0J exclusion.

`utilties.scatter2D(df, xvar, yvar, colorBy='LSP_type')` : Make a 2D scatter plot of xvar vs. yvar. The points are coloured by the `colourBy` column as follows:
* `colorBy` = `LSP_type` colors points by their LSP type.
* `colorBy` = `{A}_frac`, where `A = LSP / chi_{2,3,4}0 / chi_{1,2}p` colors the points with RGB value set to the wino/higgsino/bino fraction of the electroweakino specified by `A`.
* `colorBy` can also be set to any other column name to color the points by that column's values.

`utilities.binned2D()` : Make plots binned in two variables with bin colours corresponding to the fraction of models excluded. Options are also available to color bins by the model count and the max/min/mean of a column in the dataframe. Similar options are available for labelling the bins.

# Columns in CSV files

`Model_number` = Reference number for each model

`pMSSMFactory__model` and `pMSSMFactory__modelName` = Can be ignored. These are reference numbers for the models in the pMSSMFactory code. If you need extra information on particular models, please provide these numbers when you contact the pMSSM team.

## Final result

`Final__CLs` = Final observed CLs used to determine overall model exclusion.

--> Calculated as the observed CLs for the analysis with the best expected CLs (always using detector-level results where available)

`Final__analysis` = The name of the analysis that `Final__CLs` corresponds to

`Final__level` = Indicates the type of result the final CLs uses: `Truth` = truth-level evaluation with SimpleAnalysis, `Reco` == detector-level evaluation with RECAST, `Filtered` = Model failed pre-filter (no event production run), `Limits` = Using upper/lower limits from disappearing track, m(A) or BR(h-->invisible).

## Analysis results

For each analysis (`FullHad`, `1Lbb`, `2L0J`, `2L2J`, `Compressed`, `3LOffshell`, `3LOnshell`, `4L`, `DisappearingTrack`, `h_to_inv`, `mA`), the following columns are included:

* `{analysisName}__ExpCLs` = Expected CLs
* `{analysisName}__ObsCLs` = Observed CLs
* `{analysisName}__level` = Indicates the type of result this analysis uses: `Truth` = truth-level evaluation with SimpleAnalysis, `Reco` == detector-level evaluation with RECAST, `Filtered` = Model failed pre-filter (no event production run), `Limits` = Using upper/lower limits from disappearing track, m(A) or BR(h-->invisible).

NOTE: For `DisappearingTrack`, `h_to_inv` and `mA` only "ObsCLs" is included, and these are not "real" CLs but just a binary 0 or 1 where 0 indicates excluded and 1 indicates not excluded. For the other analyses, CLs<0.05 indicates exclusion.

## Event samples

`ML_input` = EOS path to a truth-level ntuple processed with SimpleAnalysis which contains signal region acceptance for every event for this model along with some kinematic information (in the `Clustering__ntuple` branch). Created for use in pMSSM ML studies.

`Reco_MC` = True/False to indicate if the model was processed at detector-level

`Truth_ntuple` = EOS location of the truth-level ntuple for this model, produced without a generator-level filter. 

--> Note that the original TRUTH DAOD's are not available, this column contains the path to a slimmed ntuple processed with SimpleAnalysis.

`Truth_ntuple__FILTER` = EOS location of the truth-level ntuple for this model which WAS produced with a generator-level filter. 

`DSID` = The DSID for this model. If blank then detector-level production was not run.

`AOD` = Name of the AOD produced for this model (excluding the r-tag). If blank then detector-level production was not run.

`PRW` = Name of the PRW file produced for this model. If blank then detector-level production was not run.

`EVNT` = Name of the EVNT file produced for this model. If blank then detector-level production was not run.

A generator-level filter was used to produced most of the detector-level samples, and the `Truth_ntuple__FILTER` samples. The filter used was a logical OR of:
* One lepton with pT > 15 GeV and |eta| < 2.7
* Two leptons with pT > 3 GeV and |eta| < 2.7
* MET > 100 GeV

## Model information

The column `SLHA` contains an EOS path to the SLHA file for each model (containing the mass spectrum, branching fractions etc. calculated by SPheno).

The branching fractions have also been re-calculated with `SusyHit` which, as discussed below, results in different radiative neutralino2 decay branching fractions for some models. The EOS path to the SLHA file output by `SusyHit` is under the `SH_SLHA` column.

Most of the model features you need should already be included in the columns as follows (if anything is missing please let me know):

### Mixing fractions

`LSP_Bino_frac` = Bino mixing fraction of the LSP

`LSP_Wino_frac` = Wino mixing fraction of the LSP

`LSP_Higgsino_frac` = Higgsino mixing fraction of the LSP

`chi_20_Bino_frac` = Bino mixing fraction of the second neutralino

--> Similar columns for the third and fourth neutralino and their Wino/Higgsino fractions

`chi_1p_Wino_frac` = Wino mixing fraction of the first chargino

--> Similar columns for the second chargino and their Higgsino fractions

`LSP_type` = 1 (bino fraction is largest), 2 (wino fraction is largest), 3 (higgsino fraction is largest)

### Masses

`m_chi_10` = LSP (1st neutralino) mass

`m_chi_20` = 2nd neutralino mass

`m_chi_30` = 3rd neutralino mass

`m_chi_40` = 4th neutralino mass

`m_chi_1p` = 1st chargino mass

`m_chi_2p` = 2nd chargino mass

`delta_m_chi_1p_m_chi_10` = Mass splitting between 1st chargino and LSP

`delta_m_chi_20_m_chi_10` = Mass splitting between 2nd neutralino and LSP

`m_h` = Higgs boson mass

`m_A` = Pseudoscalar Higgs boson mass

`m_W` = $W$ boson mass

### Branching fractions and total widths

The branching fraction column names have the pattern `BF_{particle_name}_to_{decay product1}_{decay_product2}`. 
The total decay widths for each electroweakino are named `width_{particle_name}`.

These branching fractions and widths were calculated by `SPheno`, but it has been found that `SusyHit` gives different values for the radiative decay of the 2nd neutralino for some models. The `SusyHit` branching fractions and widths are included with a `SH_` prefix, eg. `SH_BF_chi_20_to_chi_10_gam` and `SH_width_chi_20`.

Examples of included branching fractions and widths:

`width_chi_20` = Total neutralino2 decay width

`BF_chi_20_to_chi_1p_W` = Branching fraction for the on-shell Neutralino2 --> Chargino1 + W decay

`BF_chi_20_to_chi_10_h` = Branching fraction for the on-shell Neutralino2 --> Neutralino1 + Higgs decay

`BF_chi_20_to_chi_10_Z` = Branching fraction for the on-shell Neutralino2 --> Neutralino1 + Z decay

`BF_chi_20_to_chi_10_gam` = Branching fraction for the Neutralino2 --> Neutralino1 + photon decay

`BF_chi_20_to_chi_10_mu_mu` = Branching fraction of the off-shell Neutralino2 --> Neutralino1 + muon + antimuon decay

--> Similar naming for other off-shell Neutralino2 --> Neutralino1 + fermion + anti-fermion decays

`BF_chi_20_to_chi_1p_ele_nu_ele` = Branching fraction of the off-shell Neutralino2 --> Chargino1 + electron + neutrino decay

--> Similar naming for other off-shell Neutralino2 --> Chargino1 + lepton + neutrino decays

`BF_h_to_chi_10_chi_10` = Branching fraction for the Higgs --> Neutralino1 + Neutralino1 decay

`BF_b_to_s_gamma`        = Branching fraction for the b --> s + photon decay

`BF_Bs_to_mu_mu`         = Branching fraction for the Bs --> muon + muon decay

`BF_Bu_to_tau_nu`        = Branching fraction for the B+ --> tau + neutrino decay

### Cross-sections

All electroweak production processes were generated in proportion to the relative size of their cross-section. Therefore the event samples should be weighted using the total electroweak production cross-section: `xsec_TOTAL`. 

However, the individual cross-sections are included as follows:

`xsec_{p1}_{p2}` is the cross-section for production of particles `p1` and `p1` where the neutralinos are `10/20/30/40` and the charginos are `1p/1m/2p/2m`.

### pMSSM parameters

`M_1`      = $M_1$ (Bino mass parameter)

`M_2`      = $M_2$ (Wino mass parameter)

`mu`       = $\mu$ (Bilinear Higgs mass parameter)

`tan_beta` = $tan \beta$

`M_3`      = $M_3$ (Gluino mass parameter)

`At`       = $A_t$ (Trilinear top coupling)

`Ab`       = $A_b$ (Trilinear bottom coupling)

`Atau`     = $A_{\tau}$ (Trilinear $\tau$-lepton coupling)

`mA`       = Pseudoscalar Higgs mass parameter (note this is not necessarily the same as the physical pseudoscalar Higgs mass in column `m_A`)

`mqL1`     = Left-handed squark (first two gens.) mass parameter

`mqL3`     = Left-handed squark (third gen.) mass parameter

`muR`      = Right-handed up-type squark (first two gens.) mass parameter

`mtR`      = Right-handed top squark mass parameter

`mdR`      = Right-handed down-type squark (first two gens.) mass parameter

`mbR`      = Right-handed bottom squark mass parameter

`meL` / `mmuL` = Left-handed slepton (first two gens.) mass parameter

`meR` / `mmuR` = Right-handed slepton (first two gens.) mass parameter

`mtauL` = Left-handed stau doublet mass parameter

`mtauR` = Right-handed stau doublet mass parameter


### EW precision parameters

`delta_rho` = The delta rho EW precision parameter

`Invisible_Z_width` = The invisible Z boson decay width

### Dark matter information

`DM_relic_density` = Dark matter relic density

`SD_proton_xsec`   = Spin-dependent Neutralino1-Proton scattering cross-section

`SD_neutron_xsec`  = Spin-dependent Neutralino1-Neutron scattering cross-section

`SI_proton_xsec`   = Spin-independent Neutralino1-Proton scattering cross-section

`SI_neutron_xsec`  = Spin-independent Neutralino1-Neutron scattering cross-section

`SI_nucleon_xsec` = Spin-independent Neutralino1-Nucleon scattering cross-section

`SD_proton_xsec__SCALED` = Spin-dependent Neutralino1-Proton scattering cross-section, scaled by `DM_relic_density`/ 0.12

`SD_neutron_xsec__SCALED` = Spin-dependent Neutralino1-Neutron scattering cross-section, scaled by `DM_relic_density`/ 0.12

`SI_proton_xsec__SCALED` = Spin-independent Neutralino1-Proton scattering cross-section, scaled by `DM_relic_density`/ 0.12

`SI_neutron_xsec__SCALED` = Spin-independent Neutralino1-Neutron scattering cross-section, scaled by `DM_relic_density`/ 0.12

`SI_nucleon_xsec__SCALED` = Spin-independent Neutralino1-Nucleon scattering cross-section, scaled by `DM_relic_density`/ 0.12

### External constraint exclusion

`Constraints__Flavour` = Exclusion decision using flavour constraints including b-->s gamma, Bs-->mu mu, B+ --> tau nu. 0 = excluded, 1 = not excluded.

`Constraints__EW` = Exclusion decision using EW precision constraints including delta rho, m(W) and BR(Z->inv). 0 = excluded, 1 = not excluded.

`Constraints__DM` = Exclusion decision using dark matter constraints including both relic density and direct detection limits. 0 = excluded, 1 = not excluded.
