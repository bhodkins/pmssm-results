# dictionary mapping variable name to latex strings for axis labels

particle_names = {
	'LSP':'$\\tilde{\\mathrm{\\chi}}_{\\mathdefault{1}}^{\\mathdefault{0}}$',
	'chi_10':'$\\tilde{\\mathrm{\\chi}}_{\\mathdefault{1}}^{\\mathdefault{0}}$',
	'chi_20':'$\\tilde{\\mathrm{\\chi}}_{\\mathdefault{2}}^{\\mathdefault{0}}$',
	'chi_30':'$\\tilde{\\mathrm{\\chi}}_{\\mathdefault{3}}^{\\mathdefault{0}}$',
	'chi_40':'$\\tilde{\\mathrm{\\chi}}_{\\mathdefault{4}}^{\\mathdefault{0}}$',
	'chi_1p':'$\\tilde{\\mathrm{\\chi}}_{\\mathdefault{1}}^{\\mathdefault{+}}$',
	'chi_2p':'$\\tilde{\\mathrm{\\chi}}_{\\mathdefault{2}}^{\\mathdefault{+}}$',
	'chi_1m':'$\\tilde{\\mathrm{\\chi}}_{\\mathdefault{1}}^{\\mathdefault{-}}$',
	'chi_2m':'$\\tilde{\\mathrm{\\chi}}_{\\mathdefault{2}}^{\\mathdefault{-}}$',
	'gam':'$\\gamma$',
	'Z' : 'Z',  
	'W' : 'W',
	'h' : 'h',
	'A' : 'A',
}

masses = {'m_'+p :'$\\mathrm{m}('+particle_names[p][1:-1]+')$ [GeV]' for p in particle_names}

delta_masses = {}
xsecs = {}
for p in particle_names:
	for q in particle_names:
		delta_masses['delta_m_'+p+'_m_'+q] = '$\\Delta \\mathrm{m}('+particle_names[p][1:-1]+', '+particle_names[q][1:-1]+')$ [GeV]'
		xsecs['xsec_'+p[4:]+'_'+q[4:]] = '$\\sigma('+particle_names[p][1:-1]+particle_names[q][1:-1]+')$ pb'
xsecs['xsec_20_1pm'] = '$\\sigma(\\tilde{\\mathrm{\\chi}}_{\\mathdefault{2}^0\\tilde{\\mathrm{\\chi}}_1^{\\pm})$ pb'
xsecs['xsec_10_1pm'] = '$\\sigma(\\tilde{\\mathrm{\\chi}}_1^0\\tilde{\\mathrm{\\chi}}_1^{\\pm})$ pb'

fractions = {}
for p in ['LSP','chi_20','chi_30','chi_40','chi_1p','chi_1m','chi_2p','chi_2m']:
	for q in ['Wino','Bino','Higgsino']:
		fractions[p+'_'+q+'_frac'] = particle_names[p]+' '+q+' fraction'

BF_columns = [
'BF_chi_1p_to_chi_10', 'BF_chi_1p_to_chi_20',
'BF_chi_20_to_chi_10', 'BF_chi_20_to_chi_10_Z', 
'BF_chi_20_to_chi_10_h', 'BF_chi_20_to_chi_1p', 
'BF_chi_20_to_chi_1p_W', 'BF_chi_20_to_chi_2p',
'BF_chi_20_to_chi_2p_W', 'BF_chi_2p_to_chi_10', 
'BF_chi_2p_to_chi_1p', 'BF_chi_2p_to_chi_1p_Z',
'BF_chi_2p_to_chi_1p_h', 'BF_chi_2p_to_chi_20',
'BF_h_to_b_b', 'BF_H_to_b_b',
'BF_chi_20_to_chi_10_ele_ele','BF_chi_20_to_chi_10_mu_mu','BF_chi_20_to_chi_10_tau_tau',
'BF_chi_20_to_chi_1p_ele_nu_ele','BF_chi_20_to_chi_1p_mu_nu_mu','BF_chi_20_to_chi_1p_tau_nu_tau',
'BF_chi_20_to_chi_10_gam',
]
BFs = {}
for BF in BF_columns:
	splitBF = BF[3:].split('_to_')
	if splitBF[0] in particle_names:
		particle1 = particle_names[splitBF[0]].replace('$','')
	else:
		particle1 = splitBF[0]
	if splitBF[1] in particle_names:
		particle2 = splitBF[1]
		out = particle_names[particle2].replace('$','')
	elif splitBF[1][:-2] in particle_names:
		particle2 = splitBF[1][:-2]
		out = particle_names[particle2].replace('$','')+' + '+particle_names[splitBF[1].split('_')[-1]].replace('$','')
	elif splitBF[1][:-4] in particle_names:
		particle2 = splitBF[1][:-4]
		out = particle_names[particle2].replace('$','')+' + '+particle_names[splitBF[1].split('_')[-1]].replace('$','')
	else:
		particle2=splitBF[1].split('_')
		out = ''
		for p in particle2:
			out+=p
	
	BFs[BF] = 'BR($'+particle1+'\\rightarrow '+out+'$)'
# manually put in some extra BFs
BFs['BF_chi_20_to_chi_10_ele_ele'] = 'BR('+particle_names['chi_20']+'$\\rightarrow$'+particle_names['chi_10']+'$e^+e^-$)'
BFs['BF_chi_20_to_chi_10_mu_mu'] = 'BR('+particle_names['chi_20']+'$\\rightarrow$'+particle_names['chi_10']+'$\\mu^+\\mu^-$)'
BFs['BF_chi_20_to_chi_10_tau_tau'] = 'BR('+particle_names['chi_20']+'$\\rightarrow$'+particle_names['chi_10']+'$\\tau^+\\tau^-$)'
BFs['BF_chi_20_to_chi_10_ele_nu_ele'] = 'BR('+particle_names['chi_20']+'$\\rightarrow$'+particle_names['chi_1p']+'$e^+\\nu_e$)'
BFs['BF_chi_20_to_chi_10_mu_nu_mu'] = 'BR('+particle_names['chi_20']+'$\\rightarrow$'+particle_names['chi_1p']+'$\\mu^+\\nu_{\\mu}$)'
BFs['BF_chi_20_to_chi_10_tau_nu_tau'] = 'BR('+particle_names['chi_20']+'$\\rightarrow$'+particle_names['chi_1p']+'$\\tau^+\\nu_{\\tau}$)'
BFs['BF_h_to_chi_10_chi_10'] = 'BR$(h\\rightarrow$'+particle_names['chi_10']+particle_names['chi_10']+'$)$'
BFs['BF_h_to_inv'] = 'BR$(h\\rightarrow \\mathrm{inv.})$'
BFs['BF_b_to_s_gamma'] = 'BR$(b\\rightarrow s\\mathrm{\\gamma})$'
BFs['BF_Bs_to_mu_mu'] = 'BR$(B_s\\rightarrow \\mathrm{\\mu}\\mathrm{\\mu})$'
BFs['BF_Bu_to_tau_nu'] = 'BR$(B_u\\rightarrow \\mathrm{\\tau}\\mathrm{\\nu}_{\\mathrm{\\tau}})$'

label_map = {**particle_names, **masses,**delta_masses,**xsecs,**fractions,**BFs} 

label_map['DM_relic_density'] = '$\\Omega h^2$'
label_map['delta_rho'] = '$\\Delta\\rho$'
label_map['Invisible_Z_width'] = '$\\Gamma_{\\mathrm{inv.}}(Z)$ [MeV]'

label_map['SI_proton_xsec'] = particle_names['chi_10']+'-proton $\\sigma_{\\mathrm{SI}}$ [cm$^2$]'
label_map['SI_neutron_xsec'] = particle_names['chi_10']+'-neutron $\\sigma_{\\mathrm{SI}}$ [cm$^2$]'
label_map['SI_nucleon_xsec'] = particle_names['chi_10']+'-nucleon $\\sigma_{\\mathrm{SI}}$ [cm$^2$]'
label_map['SD_proton_xsec'] = particle_names['chi_10']+'-proton $\\sigma_{\\mathrm{SD}}$ [cm$^2$]'
label_map['SD_neutron_xsec'] = particle_names['chi_10']+'-neutron $\\sigma_{\\mathrm{SD}}$ [cm$^2$]'
label_map['SI_proton_xsec__SCALED'] = 'WIMP-proton $\\sigma_{\\mathrm{SI}}$ [cm$^2$]'
label_map['SI_neutron_xsec__SCALED'] = 'WIMP-neutron $\\sigma_{\\mathrm{SI}}$ [cm$^2$]'
label_map['SI_nucleon_xsec__SCALED'] = 'WIMP-nucleon $\\sigma_{\\mathrm{SI}}$ [cm$^2$]'
label_map['SD_proton_xsec__SCALED'] = 'WIMP-proton $\\sigma_{\\mathrm{SD}}$ [cm$^2$]'
label_map['SD_neutron_xsec__SCALED'] = 'WIMP-neutron $\\sigma_{\\mathrm{SD}}$ [cm$^2$]'

label_map['mu'] = '$\\mathrm{\\mu}$'
label_map['M_1'] = '$M_1$'
label_map['M_2'] = '$M_2$'
label_map['M_3'] = '$M_3$'
label_map['tanb_ext'] = '$\\tan\\mathrm{\\beta}$'
label_map['mA'] = '$\\mathrm{m}_{\\mathrm{A}}$ [GeV]'

label_map['Final__CLs'] = 'ATLAS Run 2 CL$_s$'
label_map['tau_chi_1p'] = '$\\tau(\\tilde{\\mathrm{\\chi}}_{\\mathdefault{1}}^{\\pm})$ [ns]'

